"""Identify modules used in a Python application by running it.
"""

__version__ = '0.1'

from .kartoffel import main
